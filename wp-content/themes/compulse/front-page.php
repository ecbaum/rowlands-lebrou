<?php

/**
 * The template for the homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package themedev
 */

get_header();
?>
<div class="bodySection">
    <div class="row">
        <div class="small-12 large-9 large-centered columns">
            <?php
            while (have_posts()) :
                the_post();

                get_template_part('template-parts/content', 'home');

            endwhile; // End of the loop.
            ?>
</div>
        </div>
        </div>

            <?php
        
            get_footer();
