<div class="header2 text-center">
  <div class="row">
    <div data-sr="enter top and scale up 10% and move 100px over .5s" class="small-12 medium-11 columns">
      <p class="headingH1">
        <?php the_field('main_text', 'options'); ?></p> 
        <?php 
         $link = get_field('header_link', 'options');
            if ($link) :
              $link_url = $link['url'];
              $link_title = $link['title'];
              $link_target = $link['target'] ? $link['target'] : '_self';
              ?>
        <a data-sr="wait 2s and enter top and scale up 10% and move 100px over 1.5s and no reset" class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_attr($link_title); ?></a>
            <?php endif; ?>
    </div>
  </div>
</div>

<?php require('topCircles.php'); ?>