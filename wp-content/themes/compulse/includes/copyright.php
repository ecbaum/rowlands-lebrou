<!-- SEO and Copyright sections -->

<div class="SEOandCopyright text-center medium-text-left">
  <div class="row">
    <div class="small-12 medium-10 columns bottomMargin20">
      <p><?php the_field('footer_copywrite','options'); ?></p>
    </div>
    <div class="small-12 medium-2 columns text-center">
      <?php
      $link = get_field('copywrite_link', 'options');
      if ($link) :
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
        ?>
        <a class="" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
          <?php $image = get_field('footer_copyright_image', 'options');

            if (!empty($image)) :
              echo echo_image($image, 'medium');
            endif; ?>

        </a>
      <?php endif; ?>
    </div>
  </div>
</div>
<!-- End SEO and Copyright sections -->