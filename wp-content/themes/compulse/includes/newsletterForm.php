<!-- Newsletter Form section -->

<div class="panel">

	<div class="row">

		<div class="small-12 medium-5 columns" style="text-align:center;">

        	<img src="<?php echo get_stylesheet_directory_uri() . 'images/pageLayout/newsletter-example.png'; ?>" alt="E-Newsletter example">

			<br>

			<h4>Rowlands & LeBrou, PLLC</h4>

			<p>Your Monthly Estate Planning Resource</p>

		</div>

		

        <div class="small-12 medium-7 columns">

			<h3 style="text-align:center;">Sign Up For Our Free<br>Monthly E-Newsletter</h3>



<!-- Begin MailChimp Signup Form -->



<div id="mc_embed_signup">

<form action="//rowlands-lebrou.us13.list-manage.com/subscribe/post?u=22cd5009a404c077a50248e03&amp;id=3aea15fe05" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	

<div class="mc-field-group">

	<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>

</label>

	<input type="text" value="" name="EMAIL" class="required email" id="mce-EMAIL">

</div>

<div class="mc-field-group input-group" id="hiddenNews">

    <strong>Subscribe me to the following: </strong>

    <ul>

    <!--<li><input type="checkbox" value="1" name="group[7009][1]" id="mce-group[7009]-7009-0"><label for="mce-group[7009]-7009-0">Blog</label></li>-->

<li><input type="checkbox" value="2" name="group[7009][2]" id="mce-group[7009]-7009-1" checked><label for="mce-group[7009]-7009-1">eNews</label></li>

</ul>

</div>

	<div id="mce-responses" class="clear">

		<div class="response" id="mce-error-response" style="display:none"></div>

		<div class="response" id="mce-success-response" style="display:none"></div>

	</div>	

    

     <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->

        

        <div style="position: absolute; left: -5000px;">

          <input type="text" name="b_b20634d1cb_49de1d90ec" value="">

        </div>

        <div class="clear">

          <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">

        </div>

      </form>

</div>



<!--End mc_embed_signup-->



<p style="text-align:center;">We NEVER share your personal information with any third parties.</p>



</div>



</div>



</div>

<!-- End Newsletter Form section -->