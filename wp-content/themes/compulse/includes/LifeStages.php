
<div class="LifeStages text-center">
    <div data-sr="enter bottom and scale up 10% and move 50px over 1.5s" class="row">
        <div class="small-11 small-centered large-12 columns">
            <p class="headingH1"><?php the_field('l_heading', 'options'); ?></p>
            <div id="circleslider">
                <div class="viewport">
                    <ul class="overview">
                        <?php
                        $rows = get_field('items', 'options');
                        $count = 0;
                        if ($rows) :
                            foreach ($rows as $row) :
                                $count++;
                                ?>
                        <li id="overview<?php echo $count; ?>">
                            <p class="headingH3"><?php echo $row['item_heading']; ?></p>
                            <p class="headingH2"><?php echo $row['second_heading']; ?></p>
                            <?php $link = $row['item_link'];
                                            if ($link) :
                                                $link_url = $link['url'];
                                                $link_title = $link['title'];
                                                $link_target = $link['target'] ? $link['target'] : '_self'; ?>
                            <a class="button" href="<?php echo esc_url($link_url); ?>"
                                target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                        </li>
                        <?php endforeach;
                        endif; ?>
                    </ul>
                </div>
                <div class="dot"></div>
                <div class="thumb"></div>
            </div>
        </div>
    </div>
</div>