<?php
add_filter('block_categories', function ($categories, $post) {
  return array_merge(
    $categories,
    array(
      array(
        'slug'  => 'compulse',
        'title' => 'Compulse Blocks',
      ),
    )
  );
}, 10, 2);



/*** Blocks ***/
function register_acf_block_types() {
  acf_register_block_type(array(
    'name'              => 'children',
    'title'             => __('Practice Area Child Pages'),
    'description'       => __('Include Children Pages'),
    'render_template'   => 'template-parts/blocks/practice_parent_page.php',
    'category'          => 'compulse',
    'icon'              => 'admin-comments',
    'keywords'          => array('children pages'),
    'mode'              => 'edit',
    'supports' => array(
      'mode' => true, // Turns off preview mode permanently
      'multiple' => false, // Does not allow multiple of this block on one page
      'anchor' => false // If true, allows custom ID
    )
  ));
 

  acf_register_block_type(array(
    'name'              => 'about-children',
    'title'             => __('About Child Pages'),
    'description'       => __('Include child pages for About'),
    'render_template'   => 'template-parts/blocks/about_parent_page.php',
    'category'          => 'compulse',
    'icon'              => 'admin-comments',
    'keywords'          => array('children pages'),
    'mode'              => 'edit',
    'supports' => array(
      'mode' => true, // Turns off preview mode permanently
      'multiple' => false, // Does not allow multiple of this block on one page
      'anchor' => false // If true, allows custom ID
    )
  ));
  acf_register_block_type(array(
    'name'              => 'employee',
    'title'             => __('Employee Block'),
    'description'       => __('Add An Employee block'),
    'render_template'   => 'template-parts/blocks/employee.php',
    'category'          => 'compulse',
    'icon'              => 'admin-comments',
    'keywords'          => array('children pages'),
    'mode'              => 'edit',
    'supports' => array(
      'mode' => true, // Turns off preview mode permanently
      'multiple' => false, // Does not allow multiple of this block on one page
      'anchor' => false // If true, allows custom ID
    )
  ));
  acf_register_block_type(array(
    'name'              => 'lifestages',
    'title'             => __('Life Style'),
    'description'       => __('lifesytle circles'),
    'render_template'   => 'includes/LifeStages.php',
    'category'          => 'compulse',
    'icon'              => 'admin-comments',
    'keywords'          => array('children pages'),
    'mode'              => 'edit',
    'supports' => array(
      'mode' => true, // Turns off preview mode permanently
      'multiple' => false, // Does not allow multiple of this block on one page
      'anchor' => false // If true, allows custom ID
    )
  ));
  acf_register_block_type(array(
    'name'              => 'newsletter',
    'title'             => __('Newsletter'),
    'description'       => __('newsletter sign up'),
    'render_template'   => 'includes/newsletterForm.php',
    'category'          => 'compulse',
    'icon'              => 'admin-comments',
    'keywords'          => array('children pages'),
    'mode'              => 'edit',
    'supports' => array(
      'mode' => true, // Turns off preview mode permanently
      'multiple' => false, // Does not allow multiple of this block on one page
      'anchor' => false // If true, allows custom ID
    )
  ));
}


// Check if function exists and hook into setup.
if (function_exists('acf_register_block_type')) {
  add_action('acf/init', 'register_acf_block_types');
}
