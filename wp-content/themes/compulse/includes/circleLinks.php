    <div class="midSection3 text-center">
      <div class="row small-up-1 medium-up-2 large-up-4">
        <?php
        $count = 0;
        $rows = get_field('circles', 'options');
        if ($rows) {
          foreach ($rows as $row) {
            if ($count <= 1) {
              $data = 'enter left and scale up 10% and move 100px over .5s';
            } else {
              $data = 'enter right and scale up 10% and move 100px over .5s';
            }
            $count++;
            $image = $row['image'];
            $link = $row['link'];
            if ($link) :
              $link_url = $link['url'];
              $link_title = $link['title'];
              $link_target = $link['target'] ? $link['target'] : '_self';
              ?>
              <div data-sr="<?php echo $data; ?>" class="column <?php echo $count; ?>">
                <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                  <div class="image-wrapper overlay-fade-in"> <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <p class="headingH2"><?php echo $row['heading']; ?></p>
                    <div class="image-overlay-content">
                      <p class="show-for-large"><?php echo $row['overlay_content']; ?></p>
                      <span class="button small"><?php echo esc_html($link_title); ?></span>
                    </div>
                  </div>
                </a>
              <?php endif; ?>
              </div>
          <?php  }
          } ?>
      </div>
    </div>