<div class="row small-up-3 topMargin10">
  <div class="column">
    <a data-toggle="share-dropdown-1" title="Share Us Online"><i class="fa fa-share-alt fa-2x"></i><span></span></a>
    <div class="dropdown-pane right text-center" id="share-dropdown-1" data-dropdown data-hover="false" data-close-on-click="true">
      <p class="headingH3">Share Us Online</p>
      <div class="row small-up-3 text-center">
        <div class="column"> <a href="http://www.facebook.com/sharer.php?u=http://www.rowlands-lebrou.com" target="_blank" title="Share Us on Facebook"><i class="fa fa-facebook fa-2x"></i></a> </div>
        <div class="column"> <a href="http://twitter.com/share?url=http://www.rowlands-lebrou.com&amp;text=Rowlands%20and%20LeBrou,%20PLLC" target="_blank" title="Share Us on Twitter"><i class="fa fa-twitter fa-2x"></i></a> </div>
        <div class="column"> <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.rowlands-lebrou.com" target="_blank" title="Share Us on LinkedIn"><i class="fa fa-linkedin fa-2x"></i></a> </div>
        <div class="column"> <a href="mailto:?Subject=I%20thought%20I'd%20share%20this%20with%20you%20-%20Rowlands%20and%20LeBrou,%20PLLC&amp;Body=http://www.rowlands-lebrou.com" target="_blank" title="Share Us by Email"><i class="fa fa-envelope fa-2x"></i></a> </div>
      </div>
    </div>
  </div>
  <div class="column"> <a href="https://www.facebook.com/pages/Rowlands-LeBrou-PLLC/204540276306006" target="_blank" title="Follow Us on Facebook"> <i class="fa fa-facebook fa-2x"></i><span></span> </a>
  </div>
  <div class="column"> <a href="https://twitter.com/RowlandsLeBrou" target="_blank" title="Follow Us on Twitter"> <i class="fa fa-twitter fa-2x"></i><span></span> </a>
  </div>
  <div class="column"> <a href="https://www.linkedin.com/company/rowlands-&-lebrou-pllc" target="_blank" title="Follow Us on LinkedIn"> <i class="fa fa-linkedin fa-2x"></i><span></span> </a>
  </div>
  <div class="column"> <a href="http://blog.rowlands-lebrou.com/" target="_blank" title="Read Our Blog"> <i class="fa fa-rss fa-2x"></i><span></span> </a> </div>
</div>