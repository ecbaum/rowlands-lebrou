<div class="horizontal-nav">
  <div class="row">
 
      <nav class="navbar navbar-expand-lg ">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon">
            <span class="icon-bar top-bar"></span>
            <span class="icon-bar middle-bar"></span>
            <span class="icon-bar bottom-bar"></span>
          </span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">

          <div class="navbar-nav text-center">
            <?php

            wp_nav_menu([
              'theme_location'    => 'primary',
              'depth'             => 2,
              'container'         => '',
              'container_class'   => '',
              'container_id'      => '',
              'menu_class'        => 'navbar-nav',
              'echo'              => true,
              'walker'            => new bs4Navwalker()
            ]);

            ?>
          </div>
        </div>
   
      </nav>

  </div>
</div>