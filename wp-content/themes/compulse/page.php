<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package themedev
 */

get_header();
?>

<div id="<?php echo strtolower(preg_replace('/\s+/', '', wp_title("", false))); ?>" class="bodySection">
    <div class="row">
        <div class="small-12 large-9 large-centered columns">
            <main id="main" class="site-main">

                <?php
                while (have_posts()) :

                    the_post();
                    get_template_part('template-parts/content', 'page');

                endwhile; // End of the loop.
                ?>
            </main><!-- #main -->
        </div><!-- #primary -->
    </div>
</div>
<?php get_footer();
