<?php

add_shortcode( 'copyright', 'copyright_info' );
function copyright_info() {

    printf( '&copy %1$d <a href="%2$s" title="%3$s">%3$s</a> | Designed by Compulse Intergrated Marketing',
	    date('Y'),
	    esc_url( home_url( '/' ) ),
	    get_bloginfo( 'name' ) );

}