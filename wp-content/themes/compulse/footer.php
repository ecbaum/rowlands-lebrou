<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package themedev
 */

?>


<footer>
    <!-- circles -->
   <?php require("includes/circleLinks.php"); ?>
 
    <div data-parallax="scroll" data-position="center bottom" data-bleed="100" data-image-src="<?php echo get_stylesheet_directory_uri() . '/images/pageLayout/mainbg5.jpg'; ?>" data-natural-width="1600" data-natural-height="899">
        <!-- lifecycle circle -->
        <?php if (is_page('home')) :
            require("includes/LifeStages.php");
           endif; ?>

        <!-- contact section -->
        <div class="midSection4 text-center">
            <div data-sr="enter bottom and scale up 10% and move 100px over .5s" class="row">
                <div class="small-12 large-9 large-centered columns">
                    <!--<p class="headingH1">Contact</p>-->
                    <p class="headingH1"><?php the_field('c_heading', 'options'); ?></p>
                    <p class="headingH3"><?php the_field('c_text', 'options'); ?></p>
                </div>
            </div>

            <div class="row">
                <div data-sr="enter left and scale up 10% and move 100px over .5s" class="small-12 medium-6 columns topMargin20">
                    <?php require("includes/contactForm.php"); ?>
                </div>
                <div data-sr="enter right and scale up 10% and move 100px over .5s" class="small-12 medium-6 columns topMargin20">
                    <div class="callout">
                        <div itemscope itemtype="http://schema.org/LegalService">
                            <p class="headingH4" itemprop="name"><?php echo get_bloginfo('name'); ?></p>
                            <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                <p>
                                    <span itemprop="streetAddress"><?php the_field('street', 'options'); ?></span>
                                    <br>
                                    <span itemprop="addressLocality"><?php the_field('city', 'options'); ?></span>,
                                    <span itemprop="addressRegion"><?php the_field('state', 'options'); ?></span>
                                    <span itemprop="postalCode"><?php the_field('zip', 'options'); ?></span>
                                </p>
                            </div>
                            <p>Phone: <span itemprop="telephone">
                                    <a href="tel:<?php echo preg_replace('/[^0-9]/', '', get_field('phone', 'options')); ?>">
                                        <?php get_field('phone', 'options'); ?><?php echo the_field('phone', 'options'); ?>
                                    </a>
                                </span></p>
                            <?php $email = get_field('email', 'options'); 
                            if($email) :?>
                            <p>Email:
                                <span itemprop="email">
                                    <a href="<?php echo antispambot('mailto:' . get_field('email', 'options')); ?>"><?php echo antispambot(get_field('email', 'options')); ?></a>
                                </span>
                            </p>
                            <?php endif;?>
                            <!--make sure to search google map for buisness, not address-->
                            <!--<a itemprop="hasMap" href="https://goo.gl/maps/UsLQb5AFjYz" target="_blank">View Map</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- twitter & social sections -->
        <div class="midSection6 text-center">
            <div class="row">
                <!--<p class="headingH1">We're Social</p>-->
                <div data-sr="enter left and scale up 10% and move 100px over .5s" class="small-12 medium-6 columns bottomMargin20">
                    <?php require("includes/twitterFeed.php"); ?>
                </div>
                <div data-sr="enter right and scale up 10% and move 100px over .5s" class="small-12 medium-6 columns">
                    <?php require("includes/socialMedia.php"); ?>
                </div>
            </div>
        </div>
    </div> <!-- end of parallax image -->

    <!-- end footer - menu -->
    <div class="footerSection text-center">
        <div class="row">


            <?php
            wp_nav_menu(array(
                'theme_location' => 'footer',
                'menu_id'        => 'menu-items',
                'container'       => 'div',
                'container_id'    => 'menu',
                'container_class' => 'small-12 columns topMargin20',
                'menu_class'      => 'vertical medium-horizontal menu',
                'walker'          => new WP_Bootstrap_Navwalker()
            ));
            ?>

        </div>
        <?php require("includes/copyright.php"); ?>
    </div>
    <a href="#0" class="cd-top">Top</a>
</footer>



<?php wp_footer(); ?>
</body>

</html>