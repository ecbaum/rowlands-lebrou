<?php
/**
 * themedev functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package themedev
 */

if ( ! function_exists( 'themedev_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function themedev_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on themedev, use a find and replace
		 * to change 'themedev' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'themedev', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * Setting for Gutenberg support. Uncomment is using block editor vs Classic and need it.
		 * Enabling theme support for align full and align wide option for the block editor, use
		 */
		//add_theme_support( 'align-wide' );

		/*
		* Add theme support for selective refresh for widgets.
		* Temp removal till I decide if I'm going to bother with customizer
		*/
		//add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		/*
		 * This theme uses 3 nav menus.
		 * Primary - Main navigation menu for desktops and most tablets
		 * Mobile - Menu dedicated  to just mobile/smaller tablets
		 * Quick - Footer links: May need to be expanded depending on site design
		//  */
		// register_nav_menus( array(
		// 	'primary-menu'   => esc_html__( 'Primary Menu', 'themedev' ),
		// 	'mobile-menu'    => esc_html__( 'Mobile Menu', 'themedev'),
		// 	'quick-menu'     => esc_html__( 'Quick Link', 'themedev')
		// ) );
		

		/**
		 * Register Bootstrap 4 Navigation Walker
		 */
		require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';
	}
endif;

require_once 'includes/blocks.php';

add_action( 'after_setup_theme', 'themedev_setup' );

register_nav_menus(array(
	'primary' => 'Primary Menu',
	'footer' => 'Footer',
	
));

require_once 'includes/client_role.php';


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function themedev_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'themedev' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'themedev' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'themedev_widgets_init' );

/**
 * Enqueue scripts and styles. 
 */
function themedev_scripts() {
	//wp_enqueue_script('themedev-jquery', get_template_directory_uri()  . '/js/jquery.min.js', array(), null, true);
	wp_enqueue_style('bootstrap4', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css');
	wp_enqueue_script('Bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js', array('jquery'), null, true);
	wp_enqueue_script('themedev-what-input', get_template_directory_uri()  . '/js/what-input.min.js', array(), null, true);
	wp_enqueue_script('font-awesome', 'https://kit.fontawesome.com/fad3c883e7.js'); 
	wp_enqueue_style( 'themedev-style', get_template_directory_uri() . '/css/style.css' );
	wp_enqueue_script( 'themedev-nav', get_template_directory_uri() . '/js/navigation.js', array(), null, true );
	wp_enqueue_script( 'themedev-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), null, true );	
	wp_enqueue_script('themedev-foundation', get_template_directory_uri()  . '/js/foundation.js', array('jquery'), null, true);
	//wp_enqueue_script('themedev-foundation-min', get_template_directory_uri()  . '/js/foundation.min.js', array('jquery'), null, true);
	wp_enqueue_script('themedev-tinycir', get_template_directory_uri()  . '/js/jquery.tinycircleslider.min.js', array('jquery'), null, true);
	wp_enqueue_script('themedev-paralex', get_template_directory_uri()  . '/js/parallax.js', array('jquery'), null, true);
	//wp_enqueue_script('themedev-paralex_min', get_template_directory_uri()  . '/js/parallax.min.js', array('jquery'), null, true);
	wp_enqueue_script('themedev-scoll', get_template_directory_uri()  . '/js/scroll-to-top.js', array('jquery'), null, true);
	wp_enqueue_script('themedev-scrollreveal', get_template_directory_uri()  . '/js/scrollReveal.js', array('jquery'), null, true);
	//wp_enqueue_script('themedev-scrollrevealmin', get_template_directory_uri()  . '/js/scrollReveal.min.js', array('jquery'), null, true);
	wp_enqueue_script('themedev-app', get_template_directory_uri()  . '/js/app.js', array('jquery'), null, true);
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'themedev_scripts' );

require_once 'bs4Navwalker.php';
// Register WordPress nav menu
register_nav_menu('top', 'Top menu');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

require_once get_template_directory() . '/inc/shortcodes.php';



// Theme Options
if (function_exists('acf_add_options_page')) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

// add excerpts 
add_action('init', 'wpse325327_add_excerpts_to_pages');
function wpse325327_add_excerpts_to_pages()
{
	add_post_type_support('page', 'excerpt');
}

add_filter('excerpt_length', function ($length) {
	return 100;
});

/********************************************
 * Image setting for sizing /alt tags on images *
 * use this: 
 *    
 * if (!empty($image)) : 
          echo echo_image($image, 'medium');      
        endif; ?>
 *
 ********************************************/

function echo_image($img, $size = 'full', $class = "img-fluid")
{
	if (is_array($img)) { //ACF image array
		$src = acf_image($img['id'], $size);
		$alt = $img['alt'];
	} else { //Assuming $img is an ID
		$src = acf_image($img, $size);
		$alt = get_post_meta($img, '_wp_attachment_image_alt', TRUE);
	}
	return '<img class="' . $class . '" alt="' . $alt . '" ' . $src . '>';
}

function acf_image($image_id, $image_size)
{
	switch ($image_size) {
		case 'thumbnail':
			$max_width = '150px';
			break;
		case 'medium':
			$max_width = '300px';
			break;
		case 'custom':
			$max_width  = '400px';
		  break;
		case 'medium_large':
			$max_width = '768px';
			break;
		case 'large':
			$max_width = '1024px';
			break;
		default:
			$max_width = '2048px';
			break;
	}

	// check the image ID is not blank
	if ($image_id != '') {
		// set the default src image size
		$image_src = wp_get_attachment_image_url($image_id, $image_size);
		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset($image_id, $image_size);
		$imgString = 'src="' . $image_src . '" ';
		if ($image_srcset) {
			$imgString .= 'srcset="' . $image_srcset . '" sizes="(max-width: ' . $max_width . ') 100vw, ' . $max_width . '"';
		}
		// generate the markup for the responsive image
		return $imgString;
	}
}

//allow vcf
function _thz_enable_vcard_upload($mime_types)
{
	$mime_types['vcf'] = 'text/vcard';
	$mime_types['vcard'] = 'text/vcard';
	return $mime_types;
}
add_filter('upload_mimes', '_thz_enable_vcard_upload');

//custom image size 

add_image_size('logo_custom', 400, 139,  true);
//add_image_size('headshot', 156, 178,  true);