jQuery(document).ready(function ($) {

 $(document).foundation();

  var config = {

    easing: 'hustle',
    reset: true,
    delay: 'onload',
    vFactor: 0,
    mobile: false,
  }

  window.sr = new scrollReveal(config);

 
  var mq = window.matchMedia('screen and (min-width: 64.063em)');
  if (mq.matches) {
    // large
    $("#circleslider").tinycircleslider({
      dotsSnap: true,
      radius: 250,
      dotsHide: false,
      interval: true,
      intervalTime: 5500
    });

  } else {
    // small screens
    $("#circleslider").tinycircleslider({
      dotsSnap: true,
      radius: 120,
      dotsHide: false,
      interval: true,
      intervalTime: 5500,
    });
  }
  
  //parent of dropdown hover open
  if (jQuery(window).width() > 768 && !isMobileDevice()) {
    jQuery('.navbar .dropdown').hover(function () {
      if (jQuery(window).width() > 768) {
        jQuery(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

      }
    }, function () {
      if (jQuery(window).width() > 768 && !isMobileDevice()) {
        jQuery(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

      }
    });
  };
  
  //parent of dropdown is clickable
  jQuery('.navbar .dropdown > a').click(function () {
    if (isMobileDevice() && window.innerWidth > 790) {
      return;
    } else {
      location.href = this.href;
    };

  });

  function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
  };



  //parent of drop down will show as active when child is open
  // if (jQuery(window).width() > 768) {
  //     jQuery('.dropdown-menu > .dropdown-item.active').closest().find('.dropdown-toggle').addClass('parent');
  // };

  if (jQuery(window).width() < 768) {
     jQuery('.dropdown-menu').addClass('show');
  };
}); 