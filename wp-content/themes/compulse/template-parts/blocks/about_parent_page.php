<?php

/**
 * Block Name: about-children
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'about-parent' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'about';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}




if (get_field('children')) :


    // Showing the list of child pages
    // Check if current page has children pages
    $children = get_pages('child_of=' . get_the_ID() . '&parent=' . get_the_ID());

    // if current page got children pages :
    if (sizeof($children) > 0) :
        $args = array(
            'post_status'       => 'publish',
            'post_type'         => 'page',
            'posts_per_page'    => -1,
            'post_parent'       => get_the_ID(),
            'order'             => 'ASC',
            'orderby'           => 'date',
        );


        $parent = new WP_Query($args);

        // Showing a list in the template
        if ($parent->have_posts()) : ?>

            <div class="row">

                <?php while ($parent->have_posts()) : $parent->the_post();
                    $id = get_the_ID();
                    $blocks = parse_blocks(get_the_content(null, false, $id));
                    foreach ($blocks as $block) {
                        if ($block['blockName'] == 'acf/employee') {
                            $title = $block['attrs']['data']['title'];
                        }
                    }
                ?>

                    <div class=" medium-4 columns">

                        <a href="<?php the_permalink(); ?>">
                            <?php if (has_post_thumbnail()) {
                                        echo echo_image(the_post_thumbnail());
                                    } ?></a><br>
                        <h4> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br>
                            <?php echo $title; ?><br>
                        </h4>
                    </div>


                <?php endwhile; ?>
                </aside>
            <?php endif; ?>

            </div>
            <?php wp_reset_query(); ?>
    <?php endif; // end if sizeof($children) 
                                                    endif;
