<?php

/**
 * Block Name: employee
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'employees' . $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'employee';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
} 
?>

<h1>About <?php the_title();?></h1>
<hr>
<h2><?php the_field('subtitle') ?></h2>
<div class="small-12 medium-4 large-4 columns">
  <h4> <?php $image = get_field('image');
      if (!empty($image)) :
        echo echo_image($image, 'medium');
      endif; ?><br>
    <?php the_field('name'); ?><br>
    <?php the_field('title'); ?><br>
  </h4>
  <a href="<?php echo antispambot('mailto:' . get_field('email')); ?>"><?php echo antispambot(get_field('email')); ?>
  </a><br>
    <?php
      $file = get_field('vcard');
      if ($file) : ?>
      <a href="<?php echo $file['url']; ?>">Download vCard Here</a>
    <?php endif; ?>
</div>
<div class="small-12 medium-8 large-8 columns">
  <p><?php the_field('info'); ?></p>
</div>