<?php

/**
 * Block Name: children
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'practice' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'practices';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
} 


if(get_field('children')):

  
// Showing the list of child pages
// Check if current page has children pages
$children = get_pages('child_of='.get_the_ID().'&parent='.get_the_ID());

// if current page got children pages :
if (sizeof($children) > 0):                 
    $args = array(
        'post_status'       => 'publish',
        'post_type'         => 'page',
        'posts_per_page'    => -1,
        'post_parent'       => get_the_ID(),
        'order'             => 'ASC',
        'orderby'           => 'date',
    );

  
    $parent = new WP_Query( $args );

    // Showing a list in the template
    if ( $parent->have_posts() ) : ?>
            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
            
                    <h2><?php the_title(); ?></h2>
                    <p><?php the_excerpt();?></p>
            <?php endwhile; ?>
        </aside>
    <?php endif; wp_reset_query(); ?>
<?php endif;// end if sizeof($children) 
endif;

