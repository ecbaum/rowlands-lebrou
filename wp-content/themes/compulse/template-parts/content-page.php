<?php

/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package themedev
 */



$id = get_the_ID();
$blocks = parse_blocks(get_the_content(null, false, $id)); ?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ((has_block('acf/employee', $id)) || (is_page('life-cycle-planning')) || (is_page('newsletter'))) {
																				$title = '';
																			} else { ?>
		<header class="entry-header">
			<?php 	//checks to see if the ACF Employee block is present on page, 
																				//and then does not print the_title() as it's pulled in from the employee block
																				$title = the_title('<h1 class="entry-title">', '</h1>');
																				echo $title;
			?>
		</header>
	<?php } ?>

	<div class="entry-content">
		<?php the_content();	?>

	
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->