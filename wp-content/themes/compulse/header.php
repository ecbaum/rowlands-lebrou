<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package themedev
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="shortcut icon" href="favicon.ico" />
    <? php // require("includes/headerScripts.php"); 
    ?>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


    <div data-parallax="scroll" data-position="center 20px" data-bleed="100" data-image-src="<?php echo get_stylesheet_directory_uri() . '/images/pageLayout/mainbg4.jpg'; ?>" data-natural-width="1600" data-natural-height="899">

        <header id="masthead" class=" header site-header">

            <?php require('includes/horizontalNav.php');
            ?>

            <div class="row">
                <?php $image = get_field('logo', 'options');
                $size = 'logo_custom';
                $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
                ?>
                <div data-sr="enter left and scale up 10% and move 100px over .5s" class="small-12 medium-7 columns topMargin20"> <a href="/">
                        <?php if ($image) {
                            echo wp_get_attachment_image($image, $size,);
                        }
                        ?>

                    </a> </div>
                <div data-sr="enter right and scale up 10% and move 100px over .5s" class="small-12 medium-5 columns text-right">
                    <p class="headingH1"><span style="background-color: rgba(23,50,80,0.8);padding: 0 .5rem;"><a href="tel:<?php echo preg_replace('/[^0-9]/', '', get_field('phone', 'options')); ?>">
                                <?php get_field('phone', 'options'); ?><?php echo the_field('phone', 'options'); ?>
                            </a></span></p>
                    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <p>
                            <span itemprop="streetAddress"><?php the_field('street', 'options'); ?></span>

                            <span itemprop="addressLocality"><?php the_field('city', 'options'); ?></span>,
                            <span itemprop="addressRegion"><?php the_field('state', 'options'); ?></span>
                            <span itemprop="postalCode"><?php the_field('zip', 'options'); ?></span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="show-for-small-only row topMargin20 bottomMargin20">
                <div class="text-center small-4 columns"> <a class="button expanded" href="tel:<?php the_field('phone', 'options'); ?>">Call</a> </div>
                <div class="text-center small-4 columns"> <a class="button expanded" href="<?php echo antispambot('mailto:' . get_field('email', 'options')); ?>">Email</a> </div>
                <div class="text-center small-4 columns"> <a class="button expanded" href="/contact-us/">Map</a> </div>
            </div>


            <div style="clear:both;"></div>


        </header><!-- #masthead -->

        <?php
        if (is_page('home')) :
            require("includes/header2.php");
        endif;
        if (!is_page('home')) :
            require("includes/header2-inside.php");
        endif; ?>

    </div>