<?php
/*
Plugin Name: Add Mime Types
 * Plugin URI: 
 * Description: You can now add .rdp files to your Wordpress
 * Version:     1.0
 * Author:      Eric Baum
 * Author URI:  
 * License:     GPLv2 or later
 * License URI: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * Text Domain: 
 * Domain Path: 
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License version 2, as published by the Free Software Foundation. You may NOT assume
 * that you can use any other version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * 
 *   --- add: 
 * php_value upload_max_filesize 64M
 *  php_value post_max_size 64M
 * php_value max_execution_time 300
 * php_value max_input_time 300


 * to the very bottom of your .htaccess file
 */

function my_custom_mime_types( $mimes ) {
     
    // New allowed mime types.
    $mimes['svg']  = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';
    $mimes['doc']  = 'application/msword'; 
 
    // Optional. Remove a mime type.
    unset( $mimes['exe'] );
 
    return $mimes;
}
 
add_filter( 'upload_mimes', 'my_custom_mime_types' );

